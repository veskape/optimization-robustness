function [GradientCouta] =GradientCouta(a,b,dimension,x,y,sigma)
GradientCouta=zeros(dimension,dimension);
for k=1:dimension
    for j=1:dimension
        sa=0;
        for i=1:30
            sa=sa+(2*x(i)*(a(j)*x(i)+b(k)-y(i)))/(sigma*sigma+(a(j)*x(i)+b(k)-y(i))*(a(j)*x(i)+b(k)-y(i)));
        end
        GradientCouta(j,k)=0.5*sa;
    end
end

