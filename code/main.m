%Auteurs: BACCHIS Charles / PALIX Xavier - FISE 2
clear all;
close all;

file = open('data.mat');

%On ouvre et renomme les variables des fichiers pour une utilisation plus
%simple dans le programme
x = file.x;
y = file.y_noisy;

%% Estimation au sens des moindres carres.

space = [-25; 50];
dimension = 100;
a = linspace(space(1), space(2), dimension);
b = a;

%% Question 1:
moindresCarres = zeros(dimension);

%Application de l'algo des moindres carrés sur l'intervalle défini
%précédement
for i=1:dimension
  for j=1:dimension
    moindresCarres(i,j) = MoindresCarres(a(i), b(j), x, y);
  end
end

figure(1);
contour(a,b,moindresCarres); 
xlabel('b'); ylabel('a');
title('Moindres carr�s');
figure(2);
mesh(a,b,moindresCarres);title('mesh moindres carr�s');

%% Question 2:
%On calcule le minimum de la matrice pour pouvoir l'afficher
[M, I] = min(moindresCarres);
[mini, col] = min(M);
row = I(col);
sampleA = (space(2) - space(1))/dimension;
sampleB = (space(2) - space(1))/dimension;
MinimumA = space(1) + row * sampleA;
MinimumB = space(1) + col * sampleB;
pointMin = [MinimumA; MinimumB];

figure(3);
contour(a, b, moindresCarres, 100); xlabel('b'); ylabel('a');
hold on;
plot(MinimumB, MinimumA, '*r');
hold off;
legend({'Moindres carr�s', 'Minimum'});
title('Moindres carr�s avec minimum calcul� en a = 7.25 et b = -1.75');

figure(4);
plot(x, MinimumB + x * MinimumA);
hold on;
plot(x, y, 'o');
hold off;
xlabel('x'); ylabel('y');
legend({'Moindres carr�s plus pr�cis', 'Points'});
title('Approximation des moindres carr�s');

%% Question 3:

xQuad = ones(size(x,1), 2);
xQuad(:, 1) = x;

% Calcul du r�sultat quadratique.
quad = inv(xQuad'*xQuad)*xQuad'*y;
aMinAcc = quad(1,1);
bMinAcc = quad(2,1);

%% Question 4:
%Affichage du minimum des moindres carr�s dans l'espace a b
figure(5);
contour(a, b, moindresCarres, 100); xlabel('b'); ylabel('a');
hold on;
plot(bMinAcc, aMinAcc, '*r');
hold off;
legend({'Moindres carr�s', 'Minimum plus pr�cis'});
title('Moindres carr�es avec le minimum en a = 6.9485 et b = -2.3803 (r�sultat quadratique)');

%% Question 5:
%Affichage de la courbe des moindres carr�s en fonctions des points donn�s
figure(6);
plot(x, bMinAcc + x * aMinAcc, 'r');
hold on;
plot(x, MinimumB + x * MinimumA, 'b');
hold on;
plot(x, y, 'o');
hold off;
xlabel('x'); ylabel('y');
legend({'Moindres carr�s quadratique', 'moindres carr� approch�s' 'Points'});
title('Approximation des moindres carr�s');

%% Question 6 :
Tempo1=ones(30, 1);
Tempo2=[x Tempo1]; % pour calculer cout Moindres carr�es et son gradient

% initialisation :

ABMC=[0.125;-2]; % Premi�re approximation de a et b pour la m�thode des moindres carr�s

Gradf=2*transpose(Tempo2)*(Tempo2*ABMC-y); % Gradient du co�t moindres carr�s

eps=0.5; %limite de pr�cision

while norm(Gradf)>=eps
    dk=-Gradf;
    alpha=FletcherLeMarechalMoindresCarres(0.5,dk,Tempo2,ABMC,Gradf,0.001,0.99,20,x,y);
    ABMC=ABMC + alpha*dk;
    Gradf=2*transpose(Tempo2)*(Tempo2*ABMC-y);
end

%% Question 7

p = @(r) (1/2)*log(1+r.^2);
pPremiereDer = @(r) r/((r.^2)+1);
pSecondeDer = @(r) (1/((r.^2)+1))-((2*r.^2)/(((r.^2)+1).^2));


figure(7);
fplot(p, [-20 20]);
hold on;
fplot(pPremiereDer, [-20 20]);
hold on;
fplot(pSecondeDer, [-20 20]);
hold off;
xlabel('r'); ylabel('p');
title('P�nalisation de Cauchy pour sigma = 1');
legend({'Fonction de p�nalisation','D�riv�e premi�re de la fonction', 'D�riv�e seconde de la fonction'});

%% Question 8 

CoutRobuste = zeros(dimension); % Initialisation matrice 100x100

for i=1:dimension
    for j=1:dimension
        s=0;
        for k=1:30
            s=s+PenalisationCauchy(a(i)*x(k)+b(j)-y(k),1);
        end
        CoutRobuste(i,j) = s;
    end
end

figure(8);
contour(a,b,CoutRobuste); 
xlabel('b'); ylabel('a');
title('Couts Robuste');
figure(9);
mesh(a,b,CoutRobuste);
xlabel('b'); ylabel('a');
title('mesh Couts Robuste');

%% Question 9

figure(10);
quiver(a,b,GradientCoutb(a,b,dimension,x,y,1),GradientCouta(a,b,dimension,x,y,1)); 
axis equal;
xlabel('b'); ylabel('a');
hold on;
contour(a,b,CoutRobuste);
title('Couts Robuste');
%% Question 10 et 11

ABCR=[0;5]; %premi�re approx de a et b. A changer pour pouvoir comparer les graphiques 
eps=0.5; % limite de pr�cision
sigma=1; % sigma de la p�nalisation de Cauchy

ABliste=zeros();
i=1;
ABliste(1,1)=ABCR(1);
ABliste(1,2)=ABCR(2);

GradCR = [AGradient(ABCR(1),ABCR(2),x,y, sigma);BGradient(ABCR(1),ABCR(2),x,y, sigma)];

while norm(GradCR)>=eps 
    i=i+1; % Compteur
    dk=-GradCR;
    alpha=FletcherLeMarechalCoutRobuste(0.05,dk,ABCR,GradCR,0.001,0.99,10,x,y,sigma);
    ABCR=ABCR + alpha*dk;
    ABliste(i,1)=ABCR(1); % Stock ai
    ABliste(i,2)=ABCR(2); % Stock bi
    GradCR=[AGradient(ABCR(1),ABCR(2),x,y, sigma);BGradient(ABCR(1),ABCR(2),x,y, sigma)];
end

row=linspace(1,i,i);

NormDist=zeros(1,i); % cr�ation d'un vecteur contenant les distance entre it�r�s en fonction de l'it�ration (2eme graphe)
for k=2:i
    NormDist(1,k)=abs(norm(ABliste(k,:))-norm(ABliste(k-1,:)));
end

CoupIter=zeros(1,i); % cr�ation d'un vecteur contenant le cout en fonction de l'it�ration (3eme graphe)
for k=1:i
    CoupIter(1,k)=CoutRobusteAB(ABliste(k,1),ABliste(k,2),x,y,1);
end
GradIter=zeros(1,i); % cr�ation d'un vecteur contenant la norme du gradient du cout en fonction de l'it�ration (3eme graphe)
for k=1:i
    GradIter(1,k)=norm([AGradient(ABliste(k,1),ABliste(k,2),x,y, sigma);BGradient(ABliste(k,1),ABliste(k,2),x,y, sigma)]);
end

DistIterFin=zeros(1,i); % cr�ation d'un vecteur contenant la distance entre chaque it�r� et la derni�re it�ration
for k=1:i
    DistIterFin(1,k)=abs(norm(ABliste(k,:))-norm(ABCR));
end

figure(11);
subplot(2,2,1)
quiver(a,b,GradientCoutb(a,b,dimension,x,y,1),GradientCouta(a,b,dimension,x,y,1)); 
axis equal;
xlim([-2 8]);
ylim([-2 8]);
xlabel('b'); ylabel('a');
hold on;
contour(a,b,CoutRobuste); 
title('It�rations avec le Co�t Robuste');
hold on;
plot(ABliste(1,2),ABliste(1,1),'-x');
hold on;
for k=2:i
    plot(ABliste(k,2),ABliste(k,1),'-x');
    line([ABliste(k-1,2), ABliste(k,2)], [ABliste(k-1,1), ABliste(k,1)], 'Color', 'r');
    xlim([-2 8])
    ylim([-2 8])      
    hold on;
end
hold off;

subplot(2,2,2)
plot(row,NormDist);
title('Distance entre les it�r�s successifs');
xlabel('i'); ylabel('AB(i)-AB(i-1)');

subplot(2,2,3)
plot(row,CoupIter,'r');
hold on;
plot(row,GradIter,'b');
title('Cout robuste (rouge) et son gradient (bleu)');
xlabel('i'); ylabel('Grad et CR');

subplot(2,2,4)
plot(row,DistIterFin);
title('Distance entre it�r�s de rang i et la derni�re it�ration');
xlabel('i'); ylabel('AB(i)-AB2');

%% Question 12
ABQN=[0;5]; %premi�re approx de a et b
eps=0.5; % limite de pr�cision
sigma=1; % sigma de la p�nalisation de Cauchy

ABlisteQN=zeros();
i=1;
ABlisteQN(1,1)=ABQN(1);
ABlisteQN(1,2)=ABQN(2);
ABQNhist = [ABQN];

GradQN = [AGradient(ABQN(1),ABQN(2),x,y, sigma);BGradient(ABQN(1),ABQN(2),x,y, sigma)];
HkInv=eye(2);

while norm(GradQN)>=eps && i < 100
    i=i+1; % Compteur
    dk=-HkInv*GradQN;
    alpha=FletcherLeMarechalCoutRobuste(0.05,dk,ABQN,GradQN,0.001,0.99,10,x,y,sigma);
    ABQN=ABQN+ alpha*dk;
    ABlisteQN(i,1)=ABQN(1); % Stock ai
    ABlisteQN(i,2)=ABQN(2); % Stock bi
    ABQNhist=[ABQNhist,ABQN];
    
    yk = [AGradient(ABQN(1),ABQN(2),x,y, sigma);BGradient(ABQN(1),ABQN(2),x,y, sigma)]-GradQN;
    GradQN=[AGradient(ABQN(1),ABQN(2),x,y, sigma);BGradient(ABQN(1),ABQN(2),x,y, sigma)];
   
    dkb=alpha*dk;
    
    HkInv=(eye(2)-(dkb*yk')/(dkb'*yk))*HkInv*(eye(2)-(yk*dkb')/(dkb'*yk))+(dkb*dkb')/(dkb'*yk);
end

figure(12);
contour(a,b,CoutRobuste,90), axis equal;
hold on;
quiver(a,b,GradientCoutb(a,b,dimension,x,y,1),GradientCouta(a,b,dimension,x,y,1)); 
hold on;
[p,q]=size(ABQNhist);
for i=1:q
    plot(ABQNhist(2,i),ABQNhist(1,i),'x r');%On affiche les r�sultats des it�rations
    if i<q
        plot([ABQNhist(2,i) ABQNhist(2,i+1)],[ABQNhist(1,i) ABQNhist(1,i+1)]);
    end
end

figure(13);
plot(1:q-1,sqrt((ABQNhist(1,1:q-1)-ABQNhist(1,2:q)).^2+(ABQNhist(2,1:q-1)-ABQNhist(2,2:q)).^2));

estimABQNhist=[];
estimgradABQNhist=[];
for j=1:q
    estimABQNhist=[estimABQNhist CoutRobusteAB(ABlisteQN(j,1),ABlisteQN(j,2),x,y,sigma)];
    estimgradABQNhist=[estimgradABQNhist norm([AGradient(ABlisteQN(j:1),ABlisteQN(j:2),x,y, sigma);BGradient(ABlisteQN(j:1),ABlisteQN(j:2),x,y, sigma)])];
end

figure(14);
subplot(2,1,1);
plot(0:q-1,estimABQNhist);
subplot(2,1,2);
plot(0:q-1,estimgradABQNhist);
figure(15);
plot(0:q-1,sqrt((ABQNhist(1,q)-ABQNhist(1,1:q)).^2+(ABQNhist(2,q)-ABQNhist(2,1:q)).^2));


%% Question 13

ABCR2=[5;-5]; % premi�re approximation de a et b
sigma2 = 1; % sigma de la p�nalisation de Cauchy à changer pour pouvoir comparer les graphiques
eps=0.5; % limite de précision

ABliste2=zeros();
ABliste2(1,1)=ABCR2(1);
ABliste2(1,2)=ABCR2(2);

GradCR2 = [AGradient(ABCR2(1),ABCR2(2),x,y, sigma2);BGradient(ABCR2(1),ABCR2(2),x,y, sigma2)];
i=1;

while norm(GradCR2)>=eps 
    i=i+1;
    dk=-GradCR2;
    alpha=FletcherLeMarechalCoutRobuste(0.5,dk,ABCR2,GradCR2,0.001,0.99,10,x,y,sigma);
    ABCR2=ABCR2 + alpha*dk;
    ABliste2(i,1)=ABCR2(1);
    ABliste2(i,2)=ABCR2(2);
    GradCR2=[AGradient(ABCR2(1),ABCR2(2),x,y, sigma2);BGradient(ABCR2(1),ABCR2(2),x,y, sigma2)];
end

row=linspace(1,i,i);

NormDist=zeros(1,i); % création d'un vecteur contenant les distance entre itéré en fonction de l'itération (2eme graphe)
for k=2:i
    NormDist(1,k)=abs(norm(ABliste2(k,:))-norm(ABliste2(k-1,:)));
end

CoupIter=zeros(1,i); % création d'un vecteur contenant le cout en fonction de l'itération (3eme graphe)
for k=1:i
    CoupIter(1,k)=CoutRobusteAB(ABliste2(k,1),ABliste2(k,2),x,y,1);
end
GradIter=zeros(1,i); % création d'un vecteur contenant la norme du gradient du cout en fonction de l'itération (3eme graphe)
for k=1:i
    GradIter(1,k)=norm([AGradient(ABliste2(k,1),ABliste2(k,2),x,y, sigma);BGradient(ABliste2(k,1),ABliste2(k,2),x,y, sigma)]);
end

DistIterFin=zeros(1,i);
for k=1:i
    DistIterFin(1,k)=abs(norm(ABliste2(k,:))-norm(ABCR2));
end

figure(16);
subplot(2,2,1)
quiver(a,b,GradientCoutb(a,b,dimension,x,y,1),GradientCouta(a,b,dimension,x,y,1)); 
axis equal;
xlim([-25 50]);
ylim([-25 50]);
xlabel('b'); ylabel('a');
hold on;
contour(a,b,CoutRobuste); 
title('Couts Robuste');
hold on;
plot(ABliste2(1,1),ABliste2(1,2),'-x');
hold on;
for k=2:i
    plot(ABliste2(k,2),ABliste2(k,1),'-x');
    line([ABliste2(k-1,2), ABliste2(k,2)], [ABliste2(k-1,1), ABliste2(k,1)], 'Color', 'r');
    xlim([-6 -1])
    ylim([1 6])      
    hold on;
end
hold off;

subplot(2,2,2)
plot(row,NormDist);
title('Distance entre it�r�s');
xlabel('i'); ylabel('AB(i)-AB(i-1)');

subplot(2,2,3)
plot(row,CoupIter,'r');
hold on;
plot(row,GradIter,'b');
title('Cout robuste (rouge) et gradient (bleu)');
xlabel('i'); ylabel('Grad et CR');

subplot(2,2,4)
plot(row,DistIterFin);
title('Distance entre it�r�s et resultat');
xlabel('i'); ylabel('AB(i)-AB2');

ABMC=[0;5]; % Première approximation initiale de a et b pour la méthode des moindres carrées
Gradf=2*transpose(Tempo2)*(Tempo2*ABMC-y); % Gradient du coût moindres carrées
eps=0.5; %limite de précision

while norm(Gradf)>=eps
    dk=-Gradf;
    alpha=FletcherLeMarechalMoindresCarres(0.5,dk,Tempo2,ABMC,Gradf,0.001,0.99,20,x,y);
    ABMC=ABMC + alpha*dk;
    Gradf=2*transpose(Tempo2)*(Tempo2*ABMC-y);
end

ABMC2=[5;-5]; % Deuxième approximation initiale de a et b pour la méthode des moindres carrées
Gradf2=2*transpose(Tempo2)*(Tempo2*ABMC2-y); % Gradient du coût moindres carrées
eps=0.5; %limite de précision

while norm(Gradf2)>=eps
    dk=-Gradf2;
    alpha=FletcherLeMarechalMoindresCarres(0.5,dk,Tempo2,ABMC2,Gradf2,0.001,0.99,20,x,y);
    ABMC2=ABMC2 + alpha*dk;
    Gradf2=2*transpose(Tempo2)*(Tempo2*ABMC2-y);
end

DroiteMC=ABMC(1)*x+ABMC(2);
DroiteMC2=ABMC2(1)*x+ABMC2(2);
DroiteRob=ABCR(1)*x+ABCR(2);
DroiteRob2=ABCR2(1)*x+ABCR2(2);

figure(17);
plot(x,DroiteMC,'r')
hold on;
plot(x,DroiteMC2,'k')
hold on;
plot(x,DroiteRob,'g')
hold on;
plot(x,DroiteRob2,'b')
hold on;
plot(x,y,'o')
title('R�sultat MC et Robuste avec sigma=1');
xlabel('x'); ylabel('y');
legend({'Droite Moindres Carr�s pour a0=0 b0=5','Droite Moindre Carr�s pour a0=5 b0=-5', 'Droite Cout Robuste pour a0=0 b0=5','Droite Cout Robuste pour a0=5 b0=-5','Points'});

%% Comparaison avec différentes valeurs de sigma

ABCR2=[0;5]; % première approximation de a et b (redéfini ABCR2 pour avoir le même point de départ)
sigma2 = 5; % sigma de la pénalisation de Cauchy (redéfini pour cette fois comparer avec 2 sigma différents)
eps=0.5; % limite de précision

ABliste2=zeros();
ABliste2(1,1)=ABCR2(1);
ABliste2(1,2)=ABCR2(2);

GradCR2 = [AGradient(ABCR2(1),ABCR2(2),x,y, sigma2);BGradient(ABCR2(1),ABCR2(2),x,y, sigma2)];
i=1;

while norm(GradCR2)>=eps 
    i=i+1;
    dk=-GradCR2;
    alpha=FletcherLeMarechalCoutRobuste(0.5,dk,ABCR2,GradCR2,0.001,0.99,10,x,y,sigma);
    ABCR2=ABCR2 + alpha*dk;
    ABliste2(i,1)=ABCR2(1);
    ABliste2(i,2)=ABCR2(2);
    GradCR2=[AGradient(ABCR2(1),ABCR2(2),x,y, sigma2);BGradient(ABCR2(1),ABCR2(2),x,y, sigma2)];
end

row=linspace(1,i,i);

NormDist=zeros(1,i); % création d'un vecteur contenant les distance entre itéré en fonction de l'itération (2eme graphe)
for k=2:i
    NormDist(1,k)=abs(norm(ABliste2(k,:))-norm(ABliste2(k-1,:)));
end

CoupIter=zeros(1,i); % création d'un vecteur contenant le cout en fonction de l'itération (3eme graphe)
for k=1:i
    CoupIter(1,k)=CoutRobusteAB(ABliste2(k,1),ABliste2(k,2),x,y,1);
end
GradIter=zeros(1,i); % création d'un vecteur contenant la norme du gradient du cout en fonction de l'itération (3eme graphe)
for k=1:i
    GradIter(1,k)=norm([AGradient(ABliste2(k,1),ABliste2(k,2),x,y, sigma);BGradient(ABliste2(k,1),ABliste2(k,2),x,y, sigma)]);
end

DistIterFin=zeros(1,i);
for k=1:i
    DistIterFin(1,k)=abs(norm(ABliste2(k,:))-norm(ABCR2));
end

figure(18);
subplot(2,2,1)
quiver(a,b,GradientCoutb(a,b,dimension,x,y,1),GradientCouta(a,b,dimension,x,y,1)); 
axis equal;
xlim([-25 50]);
ylim([-25 50]);
xlabel('b'); ylabel('a');
hold on;
contour(a,b,CoutRobuste); 
title('Couts Robuste');
hold on;
plot(ABliste2(1,2),ABliste2(1,1),'-x');
hold on;
for k=2:i
    plot(ABliste2(k,2),ABliste2(k,1),'-x');
    line([ABliste2(k-1,2), ABliste2(k,2)], [ABliste2(k-1,1), ABliste2(k,1)], 'Color', 'r');
    xlim([-1 6])
    ylim([-1 6])      
    hold on;
end
hold off;

subplot(2,2,2)
plot(row,NormDist);
title('Distance entre it�r�s');
xlabel('i'); ylabel('AB(i)-AB(i-1)');

subplot(2,2,3)
plot(row,CoupIter,'r');
hold on;
plot(row,GradIter,'b');
title('Cout robuste (rouge) et gradient (bleu)');
xlabel('i'); ylabel('Grad et CR');

subplot(2,2,4)
plot(row,DistIterFin);
title('Distance entre it�r�s et resultat');
xlabel('i'); ylabel('AB(i)-AB2');

DroiteRob=ABCR(1)*x+ABCR(2);
DroiteRob2=ABCR2(1)*x+ABCR2(2);

figure(19);
plot(x,DroiteRob,'r')
hold on;
plot(x,DroiteRob2,'b')
hold on;
plot(x,y,'o')
title('R�sultat Robuste avec diff�rent sigma');
xlabel('x'); ylabel('y');
legend({'Droite Cout Robuste pour sigma=1','Droite Cout Robuste pour sigma=5','Points'});