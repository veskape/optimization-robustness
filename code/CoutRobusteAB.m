function [CoutRobusteAB] =CoutRobusteAB(ai,bj,x,y,sigma)
    s=0;
    for k=1:30
        s=s+PenalisationCauchy(ai*x(k)+bj-y(k),sigma);
    end
    CoutRobusteAB = s;
end