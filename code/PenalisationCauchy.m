function [ro]=PenalisationCauchy(r,sigma)

ro = 0.5*log(1+(r/sigma)^2);

end
